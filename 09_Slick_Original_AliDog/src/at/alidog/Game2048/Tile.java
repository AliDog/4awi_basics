package at.alidog.Game2048;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class Tile {

	private int number; // 0; 2; 4; 8; 16; 32; 64; 128; 256; 512; 1024; 2048
	private int width = 100, height = 100;
	private int xKoord, yKoord;

	public Tile(int number, int xKoord, int yKoord) {
		super();
		this.number = number;
		this.xKoord = xKoord;
		this.yKoord = yKoord;

	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public void render(Graphics g) {

		g.drawRect(this.xKoord, this.yKoord, this.width, this.height);

		if (number > 1) {

			g.drawString(Integer.toString(number), this.xKoord, this.yKoord);
		}

	}

	// 1 2 3 4
	// 5 6 7 8
	// 9 10 11 12
	// 13 14 15 16

}
