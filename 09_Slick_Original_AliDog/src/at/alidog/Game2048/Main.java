package at.alidog.Game2048;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.util.Log;

public class Main extends BasicGame {


	private List<Tile> myTiles;
	private AppGameContainer app;
	private Random r = new Random();
	private boolean tileGenerated = false;
	private int random = 0;

	public Main() {

		super("Main");
	}

	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {

		for (Tile tile : myTiles) {

			tile.render(graphics);
		}

	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException {

		myTiles = new ArrayList<Tile>();

		int y = 30;

		for (int i = 0; i < 4; i++) {

			myTiles.add(new Tile(0, 30, y)); // 1 2 3 4
			myTiles.add(new Tile(0, 130, y));// 5 6 7 8
			myTiles.add(new Tile(0, 230, y));// 9 10 11 12
			myTiles.add(new Tile(0, 330, y));// 13 14 15 16

			y += 100;
		}
		myTiles.get(r.nextInt(16)).setNumber(2);
		myTiles.get(r.nextInt(16)).setNumber(2);

	}

	@Override
	public void update(GameContainer arg0, int delta) throws SlickException {

	}

	public void keyPressed(int key, char c) {
		if (key == Input.KEY_ESCAPE) {
			System.exit(0);
		}

		tileGenerated = false;

		if (key == Input.KEY_LEFT) {
			for (Tile tile : myTiles) {
				int position = 1;

				if (tile.getNumber() != 0) {
					Move.Left(position);
				}

				position += 1;
			}

			do {
				random = r.nextInt(16);
				if (myTiles.get(random).getNumber() == 0) {
					myTiles.get(random).setNumber(2);
					tileGenerated = true;
				}
			} while (tileGenerated == false);
		}
		if (key == Input.KEY_RIGHT) {
			for (Tile tile : myTiles) {
				int position = 1;

				if (tile.getNumber() != 0) {
					Move.Right(position);
				}

				position += 1;
			}
			do {
				random = r.nextInt(16);
				if (myTiles.get(random).getNumber() == 0) {
					myTiles.get(random).setNumber(2);
					tileGenerated = true;
				}
			} while (tileGenerated == false);
		}
		if (key == Input.KEY_DOWN) {
			for (Tile tile : myTiles) {
				int position = 1;

				if (tile.getNumber() != 0) {
					Move.Down(position);
				}

				position += 1;
			}
			do {
				random = r.nextInt(16);
				if (myTiles.get(random).getNumber() == 0) {
					myTiles.get(random).setNumber(2);
					tileGenerated = true;
				}
			} while (tileGenerated == false);
		}
		if (key == Input.KEY_UP) {
			for (Tile tile : myTiles) {
				int position = 1;

				if (tile.getNumber() != 0) {
					Move.Up(position);
				}

				position += 1;
			}
			do {
				random = r.nextInt(16);
				if (myTiles.get(random).getNumber() == 0) {
					myTiles.get(random).setNumber(2);
					tileGenerated = true;
				}
			} while (tileGenerated == false);
		}

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Main());
			container.setDisplayMode(460, 460, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
