package at.alidog.FirstGameOO;

import org.newdawn.slick.Graphics;

public class HTLRectangle {

	private int x, y;
	private int width, height;

	public HTLRectangle(int x, int y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void update(int delta) {

		if (this.x <= 740 && this.y <= 50) {
			this.x += (double) delta;
		} else if (this.x >= 600 && this.y <= 540) {
			this.y += (double) delta;
		} else if (this.x >= 10 && this.y >= 400) {
			this.x += (double) delta * (-1);
		} else if (this.x <= 10 && this.y >= 30) {
			this.y += (double) delta * (-1);
		}

	}

	public void render(Graphics g) {

		g.drawRect(this.x, this.y, this.width, this.height);

	}

}
