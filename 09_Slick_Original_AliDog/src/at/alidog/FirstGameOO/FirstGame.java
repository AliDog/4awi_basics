package at.alidog.FirstGameOO;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class FirstGame extends BasicGame {

	HTLRectangle Rectangle1 = new HTLRectangle(10, 20, 50, 50);
	HTLOval Oval1 = new HTLOval(20, 15, 30, 40);
	HTLCircle Circle1 = new HTLCircle(30, 40, 50, 50);

	public FirstGame() {

		super("FirstGame");
	}

	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {

		Rectangle1.render(graphics);
		Oval1.render(graphics);
		Circle1.render(graphics);
	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException {

	}

	@Override
	public void update(GameContainer arg0, int delta) throws SlickException {
		Rectangle1.update(delta);
		Oval1.update(delta);
		Circle1.update(delta);

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new FirstGame());
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
