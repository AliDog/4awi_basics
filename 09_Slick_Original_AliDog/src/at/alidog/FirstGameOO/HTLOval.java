package at.alidog.FirstGameOO;

import org.newdawn.slick.Graphics;

public class HTLOval {

	private int x, y;
	private int width, height;

	public HTLOval(int x, int y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void update(int delta) {

		this.y += delta;

		if (this.y == 600) {

			this.y = 0;

		}
	}

	public void render(Graphics g) {

		g.drawOval(this.x, this.y, this.width, this.height);
	}

}
