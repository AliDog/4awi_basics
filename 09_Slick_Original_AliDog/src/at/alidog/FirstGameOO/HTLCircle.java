package at.alidog.FirstGameOO;

import org.newdawn.slick.Graphics;

public class HTLCircle {

	private int x, y;
	private int width, height;
	private int direction;

	public HTLCircle(int x, int y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void update(int delta) {

		if (this.direction == 0) {

			this.x += delta;

		} else {

			this.x -= delta;

		}
		if (this.x == 800) {
			this.direction = 1;

		} else if (this.x == 0) {
			this.direction = 0;

		}
	}

	public void render(Graphics g) {

		g.drawOval(this.x, this.y, this.width, this.height);
	}

}
