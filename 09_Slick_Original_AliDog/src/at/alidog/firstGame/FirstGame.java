package at.alidog.firstGame;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class FirstGame extends BasicGame {
	int iRectangle1;
	int iRectangle2;
	int iRectDirection;

	int iCircle;
	int iCircDirection;

	int iOval;

	public FirstGame() {

		super("FirstGame");
	}

	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {

		graphics.drawRect(this.iRectangle1, this.iRectangle2, 50, 50);
		graphics.drawOval(this.iCircle, 285, 30, 30);
		graphics.drawOval(400, this.iOval, 40, 60);
	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException {

		this.iRectangle1 = 0;
		this.iRectangle2 = 50;
		this.iRectDirection = 0;

		this.iCircle = 0;
		this.iCircDirection = 0;

		this.iOval = 0;

	}

	@Override
	public void update(GameContainer gameContainer, int delta) throws SlickException {

		// Circle
		if (this.iCircDirection == 0) {

			this.iCircle += delta;

		} else {

			this.iCircle -= delta;

		}
		if (this.iCircle == 800) {
			this.iCircDirection = 1;

		} else if (this.iCircle == 0) {
			this.iCircDirection = 0;

		}

		// Oval
		this.iOval += delta;

		if (this.iOval == 600) {

			this.iOval = 0;

		}

		// Rectangle
		if (this.iRectangle1 <= 740 && this.iRectangle2 <= 50) {
			this.iRectangle1 += (double) delta;
		} else if (this.iRectangle1 >= 600 && this.iRectangle2 <= 540) {
			this.iRectangle2 += (double) delta;
		} else if (this.iRectangle1 >= 10 && this.iRectangle2 >= 400) {
			this.iRectangle1 += (double) delta * (-1);
		} else if (this.iRectangle1 <= 10 && this.iRectangle2 >= 30) {
			this.iRectangle2 += (double) delta * (-1);
		}

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new FirstGame());
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
