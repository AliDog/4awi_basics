package at.alidog.car;

public class Car {

	private double maxSpeed;
	private double basicPrice;
	private double fuelConsumption;
	private double km;
	private Producer producer;
	private Engine engine;

	public Car(double maxSpeed, double basicPrice, double fuelConsumption, double km, at.alidog.car.Producer producer,
			at.alidog.car.Engine engine) {
		super();
		this.maxSpeed = maxSpeed;
		this.basicPrice = basicPrice;
		this.fuelConsumption = fuelConsumption;
		this.km = km;
		this.producer = producer;
		this.engine = engine;
	}

	public double getPrice() {
		double basic;
		double discount;
		double price;
		double discount2;

		basic = basicPrice;
		discount = producer.getDiscount();
		discount2 = basic * discount;
		price = basic - discount2;

		return price;
	}

	public String getType() {
		String Type = engine.getType();

		return Type;
	}

	public double getTruefuelConsumption() {

		if (km > 50000) {
			fuelConsumption = fuelConsumption * 1.098;

		}

		return fuelConsumption;
	}

}
