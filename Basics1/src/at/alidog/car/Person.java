package at.alidog.car;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class Person {

	private String name;
	private LocalDate birthday;
	private List<Car> cars;

	public Person(String name, LocalDate birthday) {
		super();
		this.name = name;
		this.birthday = birthday;
		cars = new ArrayList<Car>();
	}

	public void addCar(Car car) {
		cars.add(car);
	}

	public double GetValuesOfCArs() {

		double value = 0;

		for (Car car : cars) {
			value = value + car.getPrice();

		}

		return value;
	}

	public int GetAge() {
		int age = 0;

		age = Period.between(this.birthday, LocalDate.now()).getYears();

		return age;
	}

}
