package at.alidog.car;

public class Engine {

	private String type;
	private double performance;

	public Engine(String type, double performance) {
		super();
		this.type = type;
		this.performance = performance;
	}

	public String getType() {
		return type;
	}

}
