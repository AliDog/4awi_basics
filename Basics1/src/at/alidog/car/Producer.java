package at.alidog.car;

public class Producer {

	private String name;
	private String location;
	private double discount;

	public Producer(String name, String location, double discount) {
		super();
		this.name = name;
		this.location = location;
		this.discount = discount;
	}

	public String getName() {
		return name;
	}

	public double getDiscount() {
		return discount;

	}
}