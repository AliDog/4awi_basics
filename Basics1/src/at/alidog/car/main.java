package at.alidog.car;

import java.time.LocalDate;

public class main {

	public static void main(String[] args) {

		Producer p1 = new Producer("Aston Martin", "Britian", 0.20);
		Engine e1 = new Engine("Diesel", 470);
		Car c1 = new Car(300, 200000, 10, 60000, p1, e1);
		double price;
		String type;
		double consumption;

		Producer p2 = new Producer("Tesla", "USA", 0.05);
		Engine e2 = new Engine("Electric", 550);
		Car c2 = new Car(250, 200000, 15, 100, p2, e2);

		price = c1.getPrice();
		type = c1.getType();
		consumption = c1.getTruefuelConsumption();

		System.out.println(p1.getName());
		System.out.println("Price: " + price + "�");
		System.out.println("Type: " + type);
		System.out.println("Fuel Consumption: " + consumption + "l/100km");

		price = c2.getPrice();
		type = c2.getType();
		consumption = c2.getTruefuelConsumption();

		System.out.println();
		System.out.println(p2.getName());
		System.out.println("Price: " + price + "�");
		System.out.println("Type: " + type);
		System.out.println("Fuel Consumption: " + consumption + "kWh/100km");

		Person ps1 = new Person("Ali Doganyigit", LocalDate.parse("2001-02-22"));

		ps1.addCar(c1);
		ps1.addCar(c2);

		System.out.println();
		System.out.println("Age: " + ps1.GetAge());
		System.out.println("Value of cars: " + ps1.GetValuesOfCArs());

	}

}
