package at.alidog.rectangle;

public class Rectangle {
	private int a;
	private int b;

	public Rectangle(int a, int b) {

		this.a = a;
		this.b = b;
	}

	public void sayHello() {
		System.out.println("ich bin " + this.a + " breit und " + this.b + " lang");

	}

	public double getArea(int a, int b) {
		double area = a * b;

		return area;
	}

}
