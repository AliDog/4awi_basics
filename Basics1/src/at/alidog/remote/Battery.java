package at.alidog.remote;

public class Battery {

	private double chargingStatus;

	public double getChargingStatus() {
		return chargingStatus;
	}

	public Battery(double chargingStatus) {
		this.chargingStatus = chargingStatus;
	}

	public void setChargingStatus(double chargingStatus) {
		this.chargingStatus = chargingStatus;
	}

}
