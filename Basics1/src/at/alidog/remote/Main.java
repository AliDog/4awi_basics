package at.alidog.remote;

public class Main {

	public static void main(String[] args) {

		Battery b1 = new Battery(49);
		Battery b2 = new Battery(60);

		Remote r1 = new Remote(true, b1);
		Remote r2 = new Remote(true, b2);

		double status;

		r1.turnOff();

		r1.turnOn();

		status = r1.hasPower();

		if (status > 50) {

			System.out.println("The Remote alot Power");
		} else {
			System.out.println("The Remote just a little Power");
		}

		status = r2.hasPower();

		if (status > 50) {

			System.out.println("The Remote alot Power");
		} else {
			System.out.println("The Remote just a little Power");
		}

	}

}
