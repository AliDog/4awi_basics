package at.alidog.remote;

public class Remote {

	private boolean isOn;
	private Battery battery;

	public Remote(boolean isOn, Battery battery) {
		super();
		this.isOn = isOn;
		this.battery = battery;
	}

	public boolean isOn() {
		return isOn;
	}

	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}

	public Battery getBattery() {
		return battery;
	}

	public void setBattery(Battery battery) {
		this.battery = battery;
	}

	public void turnOn() {

		isOn = true;
		System.out.println("The Remote is now turned on");

	}

	public void turnOff() {

		isOn = false;
		System.out.println("The Remote is now turned off");

	}

	public double hasPower() {

		double b1;

		b1 = battery.getChargingStatus();

		return b1;
	}

}
